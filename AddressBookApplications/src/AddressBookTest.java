import static org.junit.Assert.*;

import org.junit.Test;

public class AddressBookTest {

	@Test
	public void testNewEntry() {
		AddressBook contact = new AddressBook();
		AddressEntry a = new AddressEntry();
		contact.newEntry ("Liya", "Norng", "311 B street", "Tracy","CA.","95376", "Liya_Norng@yahoo.com", "(209) 346-2998");
		assertEquals(contact.count("Norng"), 1);
		contact.newEntry("Kado", "Norng", "311 B street", "Tracy","CA.","95376", "kadonorng@yahoo.com", "(209) 346-2998");
		assertEquals(contact.count("Norng"), 2);
		contact.newEntry("Kanal", "Norng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		assertEquals(contact.count("Norng"), 3);
	}
	
	@Test
	public void testCount() {
		AddressBook contact = new AddressBook();
		contact.newEntry("Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		assertEquals(1,contact.count("Seng"));	
	    contact.newEntry("Kanal", "Norng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		assertEquals(1,contact.count("Norng"));

	}

	@Test
	public void testGetFirstNames() {
		AddressBook contact = new AddressBook();
		contact.newEntry("Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		contact.count("Seng");
		contact.getContact("Seng", 1);
		assertEquals("Kanal", contact.getFirstNames());
		
	}

	@Test
	public void testGetLastNames() {
		AddressBook contact = new AddressBook();
		contact.newEntry("Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
        contact.count("Seng");
		contact.getContact("Seng", 1);
		assertEquals("Seng", contact.getLastNames());	
		}

	@Test
	public void testGetContact() {
		AddressBook contact = new AddressBook();
		contact.getContact("Seng", 1);
	}

}
