/**
 * @author Liya Norng
 * @version 1.0
 * @since 1/20/2016
 */

/**
 * this is the class AddressEntry. 
 * @param first_Name is first name for storing the information.
 * @param last_Name is last name for storing the information.
 * @param street_Name is street name for storing the information.
 * @param city_Name is city name for storing the information.
 * @param state_Name is state name for storing the information.
 * @param zip_Name is the zip code for storing the information.
 * @param email_Name is the email for storing the information.
 * @param phone_Name is the phone number for storing the information.
 */
public class AddressEntry {
	// Variables 
	String first_Name;
	String last_Name;
	String street_Name;
	String city_Name;
	String state_Name;
	String zip_Name;
	String email_Name;
	String phone_Name;
	
	/**
	 * this is the constructor that accepts a first, last name, street, city, 
	 * state, zip, email, phone.
	 */
	//constructor…one that accepts a first, last name, street, city, state, zip, email, phone.
	AddressEntry (String first_Name, String last_Name, String street_Name, String city_Name, String state_Name, String zip_Name, String email_Name, String phone_Name )
	{
		this.first_Name = first_Name;
		this.last_Name = last_Name;
		this.street_Name = street_Name;
		this.city_Name = city_Name;
		this.state_Name = state_Name;
		this.zip_Name = zip_Name;
		this.email_Name = email_Name;
		this.phone_Name = phone_Name;
	}
	
	/**
	 * this is just a dummy constructor, which I need for one of the class, because the other constructor need attributes.
	 */
	AddressEntry ()
	{
		
	}
	
	/**
	 * @return string of contacts as a nicely format. 
	 */
	public String toString()
	{
		return (first_Name + " " +  last_Name + "\n" + street_Name + "\n" + city_Name + ", " + state_Name + " " + zip_Name + "\n" + email_Name + "\n" + phone_Name);
	}
	
	/**
	 * @return string of contacts as a nicely format. 
	 */
	public String toFile()
	{
		return (first_Name + last_Name + "\n" + street_Name + "\n" + city_Name + "," + state_Name + zip_Name + "\n" + email_Name + "\n" + phone_Name);
	}
	
	/**
	 * all of this methods would set all the string to appropriate string.
	 */
	public void setFirstName(String first_Name)
	{
		this.first_Name = first_Name;
	}
	public void setLastName(String last_Name)
	{
		this.last_Name = last_Name;
	}
	public void setStreet(String street_Name)
	{
		this.street_Name = street_Name;
	}
	public void setCity(String city_Name)
	{
		this.city_Name = city_Name;
	}
	public void setState(String state_Name)
	{
		this.state_Name = state_Name;
	}
	public void setZip(String zip_Name)
	{
		this.zip_Name = zip_Name;
	}
	public void setEmail(String email_Name)
	{
		this.email_Name = email_Name;
	}
	public void setPhone(String phone_Name)
	{
		this.phone_Name = phone_Name;
	}
	/**
	 * all of this methods would return the string according to the methods name.
	 * @return first_Name this value is to return to the request of the class.
	 * @return last_Name this value is to return to the request of the class.
	 * @return street_Name this value is to return to the request of the class.
	 * @return city_Name this value is to return to the request of the class.
	 * @return state_Name this value is to return to the request of the class.
	 * @return zip_Name this value is to return to the request of the class.
	 * @return email_Name this value is to return to the request of the class.
	 * @return phone_Name this value is to return to the request of the class.
	 */
	public String getFirstName()
	{
		return first_Name;
	}
	public String getLastName()
	{
		return last_Name;
	}
	public String getStreet()
	{
		return street_Name;
	}
	public String getCity()
	{
		 return city_Name;
	}
	public String getState()
	{
		return state_Name;
	}
	public String getZip()
	{
		return zip_Name;
	}
	public String getEmail()
	{
		return email_Name;
	}
	public String getPhone()
	{
		return phone_Name;
	}
	
	/**
	 * 
	 * @param contact is a AddressBook class. this method is to add in new entry by calling to the AddressBook Application.
	 */
	public void addContact(AddressBook contact)
	{
		contact.newEntry(first_Name, last_Name, street_Name, city_Name, state_Name, zip_Name, email_Name, phone_Name);
	}
	
	
}
