import static org.junit.Assert.*;
import org.junit.Test;


public class AddressEntryTest {

	@Test
	public void testAddressEntryStringStringStringStringStringStringStringString() {
	}

	@Test
	public void testAddressEntry() {
		AddressEntry contact = new AddressEntry("Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		String s =  ("Kanal" + " " +  "Seng" + "\n" + "123 A Street" + "\n" + "Tracy" + ", " + "CA." + " " + "95376" + "\n" + "Kanalnorng@yahoo.com" + "\n" + "(209) 346-2998");
		assertEquals(contact.toString(), s);
	}

	@Test
	public void testToString() {
		AddressEntry contact = new AddressEntry("Kanal", "Seng", "123 A Street", "Tracy","CA.","95376", "Kanalnorng@yahoo.com", "(209) 346-2998");
		String s =  ("Kanal" + " " +  "Seng" + "\n" + "123 A Street" + "\n" + "Tracy" + ", " + "CA." + " " + "95376" + "\n" + "Kanalnorng@yahoo.com" + "\n" + "(209) 346-2998");
		assertEquals(contact.toString(), s);
		
	}

	

	@Test
	public void testSetFirstName() {
		AddressEntry contact = new AddressEntry();
		contact.setFirstName("Liya");
		assertEquals("Liya", contact.getFirstName());
		contact.setFirstName("Kado");
		assertEquals("Kado", contact.getFirstName());	
		}

	@Test
	public void testSetLastName() {
		AddressEntry contact = new AddressEntry();
		contact.setLastName("Norng");
		assertEquals("Norng", contact.getLastName());
		contact.setLastName("Seng");
		assertEquals("Seng", contact.getLastName());
		}

	@Test
	public void testSetStreet() {
		AddressEntry contact = new AddressEntry();
		contact.setStreet("311 B Street");
		assertEquals("311 B Street", contact.getStreet());
		contact.setStreet("588 B Street");
		assertEquals("588 B Street", contact.getStreet());
		}

	@Test
	public void testSetCity() {
		AddressEntry contact = new AddressEntry();
		contact.setCity("Tracy");
		assertEquals("Tracy", contact.getCity());
		contact.setCity("Hayward");
		assertEquals("Hayward", contact.getCity());	
		}

	@Test
	public void testSetState() {
		AddressEntry contact = new AddressEntry();
		contact.setState("Ca");
		assertEquals("Ca", contact.getState());
		contact.setState("Tx");
		assertEquals("Tx", contact.getState());	}

	@Test
	public void testSetZip() {
		AddressEntry contact = new AddressEntry();
		contact.setZip("12345");
		assertEquals("12345", contact.getZip());
		contact.setZip("09876");
		assertEquals("09876", contact.getZip());	
		}

	@Test
	public void testSetEmail() {
		AddressEntry contact = new AddressEntry();
		contact.setEmail("Liya_Norng@yahoo.com");
		assertEquals("Liya_Norng@yahoo.com", contact.getEmail());
		contact.setEmail("Kado_Norng@yahoo.com");
		assertEquals("Kado_Norng@yahoo.com", contact.getEmail());	
		}

	@Test
	public void testSetPhone() {
		AddressEntry contact = new AddressEntry();
		contact.setPhone("209-234-2222");
		assertEquals("209-234-2222", contact.getPhone());
		contact.setPhone("334-343-2342");
		assertEquals("334-343-2342", contact.getPhone());
		}
}
